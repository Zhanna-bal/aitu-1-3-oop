package kz.aitu.oop.examples.practic7;

import java.util.Scanner;

public class Main {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            FoodFactory fF = new FoodFactory();
            Food food = fF.getFood(sc.nextLine());
            System.out.println("The factory returned " + food.getClass());
            System.out.println(food.getType());
        }
    }


