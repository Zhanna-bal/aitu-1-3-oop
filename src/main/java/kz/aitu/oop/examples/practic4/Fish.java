package kz.aitu.oop.examples.practic4;

public class Fish {


    private static Integer count = new Integer(0);

    protected String name;
    protected Integer number;

    // Constructor
    protected Fish(String name) {
        this.name = name;

        this.number = count;

        count++;
    }

    // Show some stuff about the class.
    protected void show() {
        System.out.println(number.toString() +
                ". " + this.name);
    }

}