package kz.aitu.oop.examples.practic4;

public class Aquarium {
    protected double price = 0;
    protected double weight = 0;
    private static double totalPrice = 0;
    private static double totalWeight = 0;


    public Aquarium(double price, double weight) {
        this.price = price;
        this.weight = weight;
        totalPrice += this.price;
        totalWeight += this.weight;
    }

    public Aquarium(double weight) {
        this.weight = weight;
        totalWeight += this.weight;
    }

    public static double getTotalPrice() { return totalPrice; }

    public static double getTotalWeight() { return totalWeight; }

}
