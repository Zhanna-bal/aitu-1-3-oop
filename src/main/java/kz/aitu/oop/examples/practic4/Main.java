package kz.aitu.oop.examples.practic4;

import static kz.aitu.oop.examples.practic4.Aquarium.getTotalPrice;
import static kz.aitu.oop.examples.practic4.Aquarium.getTotalWeight;

public class Main {
    public static void main(String[] args) {

        Aquarium ob1 = new Aquarium(2, 5);
        Aquarium ob2 = new Aquarium(5);

        Fish fish1 = new Fish("Lisa");
        Fish fish2 = new Fish("Pete");
        Fish fish3 = new Fish("Richard");

        fish1.show();
        fish2.show();
        fish3.show();


        Reptile reptile1 = new Reptile(5,3,"Snake");
        Reptile reptile2 = new Reptile(5,2);
        Reptile reptile3 = new Reptile(5,"Crocodile");
        Reptile reptile4 = new Reptile(5);



        System.out.println("Your total price: " + getTotalPrice() + " and total height: " + getTotalWeight());
    }

}
