package kz.aitu.oop.examples.practic4;

public class Reptile extends Aquarium {

        String nameOfReptile;

        public Reptile(double price, double weight, String nameOfReptile) {
            super(price, weight);
        }

        public Reptile(double price, double weight) {
            super(price, weight);
        }

        public Reptile(double weight) {
            super(weight);
        }

        public Reptile(double weight, String nameOfReptile) {
            super(weight);
        }


}
