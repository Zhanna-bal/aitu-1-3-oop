package kz.aitu.oop.examples.inheritance;

public class MovablePoint {
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }
 
    public void moveUp() {
        y++;
    }
 
    public void moveDown() {
        y--;
    }

    public void moveLeft() {
        x--;
    }
    
    public void moveRight() {
        x++;
    }

    public String toString(){
        return "x = " + this.x + ", y = " + this.y + ", x speed is " + this.xSpeed + ", y speed is " + this.ySpeed;
    }



    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
    }

    public void setxSpeed(int xSpeed) {
    }

  public void setySpeed(int ySpeed){
        
  }

    public void gety() {
    }

    public int gety(double i) {
        return (int) i;
    }

    public void getySpeed() {

    }
    public void getxSpeed() {

    }
}
