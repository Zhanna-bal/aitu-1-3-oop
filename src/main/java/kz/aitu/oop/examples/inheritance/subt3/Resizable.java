package kz.aitu.oop.examples.inheritance.subt3;

public interface Resizable {
    void resize(int percent);
}
