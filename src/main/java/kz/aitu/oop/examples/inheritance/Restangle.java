package kz.aitu.oop.examples.inheritance;

public class Restangle extends Shape {
    private double widthh;
    private double length;

    public Restangle(){
        super();
        widthh = 1.0;
        length = 1.0;
    }

    public Restangle(double widthh, double length) {
        super();
        this.widthh = widthh;
        this.length = length;
    }

    public Restangle(double widthh,double length,String color,boolean filled){
         super(color,filled);
         this.widthh = widthh;
         this.length = length;
    }

    public double getWidthh() {
        return widthh;
    }

    public void setWidthh(double widthh) {
        this.widthh = widthh;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return length*widthh;
    }

    public double getPerimeter(){
        return (2*length)+(2*widthh);
    }
    @Override
    public String toString(){
        return "A Restangle with width=" + widthh + "and length" + length + "which is a subclass of " + super.toString();
    }

}
