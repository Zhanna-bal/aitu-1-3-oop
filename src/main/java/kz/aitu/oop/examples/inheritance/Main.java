package kz.aitu.oop.examples.inheritance;

import kz.aitu.oop.examples.encapsulation.Account;

public class Main {
    public static void main(String[] args) {
        Shape shape = new Shape();
        Circle circle = new Circle();
        Restangle restangle = new Restangle();
        Square square = new Square();
        System.out.println(circle.getPerimeter());
        System.out.println(shape.getColor());
        System.out.println(restangle.toString());
        System.out.println(square.getSide());
        System.out.println(shape.isFilled());
        shape.getColor();
        square.getArea();
    }}
