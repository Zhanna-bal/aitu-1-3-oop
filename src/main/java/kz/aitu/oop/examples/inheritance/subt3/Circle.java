package kz.aitu.oop.examples.inheritance.subt3;

public class Circle implements GeomericObject {

    private double radius;


    public Circle() {
        super();
        radius = 1.0;
    }

    public Circle(double radius){
        super();
        this.radius = radius;
    }

    @Override
    public double getPermitere() {
        return (this.radius * 2) * 3.14;
    }

    @Override
    public double getArea() {
        return Math.pow(this.radius, 2) * 3.14;
    }

    public String toString(){
        return "radius is " + this.radius ;
    }

    protected void getRadius() {
    }
    protected void setRadius(){

    }
}
