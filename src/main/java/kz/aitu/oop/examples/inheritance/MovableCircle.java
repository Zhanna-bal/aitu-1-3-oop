package kz.aitu.oop.examples.inheritance;

public class MovableCircle implements Movable {

        private int radius;
        private MovablePoint center;

        public MovableCircle(int x1, int y1, int xS, int yS, int radius) {
            center.setX(x1);
            center.setY(y1);
            center.setxSpeed(xS);
            center.setySpeed(yS);
            this.radius = radius;
        }

        @Override
        public void moveUp() {
            center.setY(center.gety(1));
        }

        @Override
        public void moveDown() {
            center.setY(
                    center.gety(-1));
        }

        @Override
        public void moveLeft() {
            center.setX(center.getX()-1);
        }

        @Override
        public void moveRight() {

            center.setX(center.getX()+1);
        }

        public String toString(){
            return "x = " + center.getX() + ", y = " + center.gety() + ", x speed is " + center.getxSpeed() + ", y speed is " + center.getySpeed() + ", radius is " + this.radius;
        }
    }

}
