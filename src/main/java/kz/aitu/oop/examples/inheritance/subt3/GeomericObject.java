package kz.aitu.oop.examples.inheritance.subt3;

public interface GeomericObject {

    double getPermitere();

    double getArea();
}
