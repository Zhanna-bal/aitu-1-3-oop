package kz.aitu.oop.examples.practic2;

public class Train {
    private  int numberOfPassengers;
    private double carrying_capacity;

    public Train(int numberOfPassengers, double carrying_capacity) {
        this.numberOfPassengers=numberOfPassengers;
        this.carrying_capacity=carrying_capacity;
    }

    public Train() {
        numberOfPassengers=36;
        carrying_capacity=530.0;
    }

    public void setNumberOfPassengers(int numberOfPassengers){
        this.numberOfPassengers=numberOfPassengers;
    }

    public int getNumberOfPassengers(){
        return numberOfPassengers;
    }
    public void setCarrying_capacity(double carrying_capacity){
        this.carrying_capacity=carrying_capacity;
    }

    public double getCarrying_capacity(){
        return carrying_capacity;
    }



    private class Car {
        String name;                 // name of train car
        Car next;                   // link to next train car

        // constructor for train cars
        private Car(String initialName) {
            name = initialName;

        }
    }

    private Car first = null;

    public void threeCars() {
        first = new Car("Locomotive");
        first.next = new Car("Cafe");

    }

    public void printAll() {

        Car current = first;


        while (current != null) {

            System.out.println(current.name);
            current = current.next;
        }
    }


    public void insertAtEnd(String carName) {
        Car newCar = new Car(carName);
        if (first == null)
            first = newCar;
        else {
            Car current = first;
            while (current.next != null)
                current = current.next;
            current.next = newCar;
        }


    }



}
