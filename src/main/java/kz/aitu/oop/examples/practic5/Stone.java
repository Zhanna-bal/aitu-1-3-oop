package kz.aitu.oop.examples.practic5;

public class Stone {
    protected double price = 0;
    protected double weight = 0;
    private static double totalPreciousPrice = 0;
    private static double totalPreciousWeight = 0;


    public Stone(double price, double weight) {
        this.price = price;
        this.weight = weight;
        totalPreciousPrice += this.price;
        totalPreciousWeight += this.weight;
    }
    }
