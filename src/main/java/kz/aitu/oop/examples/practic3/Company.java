package kz.aitu.oop.examples.practic3;

public class Company {
    private String companyName;
    private String projectName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }



    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return "Company Name: " + companyName +
                "\nProject Name: " + projectName
                ;
    }



}
