package kz.aitu.oop.examples.practic3;

public class Employee {
    private int salary;

    public Employee (int salary){
        this.salary=salary;
    }

    public Employee() {

    }

    public void setSalary(int salary){
        this.salary=salary;
    }

    public int getSalary(String s){
        return salary;
    }
}
