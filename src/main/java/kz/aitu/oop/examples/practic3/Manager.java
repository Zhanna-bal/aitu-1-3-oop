package kz.aitu.oop.examples.practic3;

public class Manager extends Employee {
    public Manager(int salary) {
        super(salary);
    }
    private String Manager;

    public Manager() {
        super();
    }

    public void setManager(String manager) {
        Manager = manager;
    }

    public String getManager() {
        return Manager;
    }

}
