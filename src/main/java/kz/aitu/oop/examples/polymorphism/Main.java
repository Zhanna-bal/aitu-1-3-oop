package kz.aitu.oop.examples.polymorphism;

public class Main {
    public static void main(String[] args){
    Animal animal = new Animal("animal 1");
    System.out.println(animal.getName());
    animal.eat();
        System.out.println("///////////");
        Dog dog = new Dog("Dog1");

        dog.eat();
        dog.run();

    }

}
