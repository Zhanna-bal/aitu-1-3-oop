package kz.aitu.oop.examples.polymorphism;

public class Dog extends Animal {
    public Dog(String name) {
        super(name);

    }
    public void run() {
        System.out.println("Dog"+ this.getName() + "run");

    }
     public void eat() {
        System.out.println("Dog is eating" + this.getName());
     }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
