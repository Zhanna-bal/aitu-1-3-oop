package kz.aitu.oop.examples.pract6;

public class Singleton {
    public String str;
    private static Singleton singleton = null;

    private Singleton() {
    }
    public static Singleton getSingleInstance() {

        if(singleton == null) {
            singleton = new Singleton();
        }

        return singleton;
    }

}
