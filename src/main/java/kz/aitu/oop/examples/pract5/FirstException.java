package kz.aitu.oop.examples.pract5;

public class FirstException {
    public FirstException(String message) {
        message = "This is bound to execute";
        System.out.println(message);
    }

    public static void main(String[] args) throws Exception {
        try {
            throw new FileNotFoundException();
        }
        catch (FileNotFoundException e) {
            throw new Exception("File not found");
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            System.out.println("I was there here");
        }

    }
}

}
