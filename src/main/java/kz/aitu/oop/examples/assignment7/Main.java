package kz.aitu.oop.examples.assignment7;

public class Main {
    public static void main(String[] args) {
        Circle s1 = new Circle(5.5, "red", false);
        System.out.println(s1);// because s1 is from Shape class and it doesn't have this function
        //because s1 is from Shape class and it doesn't have this function
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());
        //besause s1 is from Shape class and it doesn't have this function

        Circle c1 = s1;

        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());


        Shape s2 = new Shape() {
            @Override
            public double getArea() {
                return 0;
            }

            @Override
            public double getPerimeter() {
                return 0;
            }

            @Override
            public double getLength() {
                return 0;
            }
        };

        Shape s3 = new Restangle(1.0, 2.0, "red", false);

        System.out.println(s3);
        //because s3 is from Shape class and it doesn't have this function
        //because s3 is from Shape class and it doesn't have this function
        System.out.println(s3.getColor());
        //because s3 is from Shape class and it doesn't have this function


        Restangle r1 = (Restangle)s3;

        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());


        Shape s4 = new Square(6.6);

        System.out.println(s4);
        //because s4 is from Shape class and it doesn't have this function
        System.out.println(s4.getColor());
        //because s4 is from Shape class and it doesn't have this function


        Restangle r2 = (Restangle)s4;

        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
        //because r2 is from Rectangle class and it doesn't have this function
        System.out.println(r2.getLength());


        Square sq1 = (Square)r2;

        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());
    }

}
