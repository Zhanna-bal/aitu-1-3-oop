package kz.aitu.oop.examples.assignment7;

public class Square extends Restangle {
    private double side;

    public Square() {
        super();
        double side = 1.0;

    }

    public Square (double side)
    {
        super(side,side);
        side = side;

    }

    public Square(double side, String color ,boolean filled) {
        super(side,side,color,filled);
        side = side;

    }

    public double getSide() {
        return super.getWidth();
    }


    public double getArea() {
        return getSide()*getSide();
    }

    public double getPerimeter() {
        return 4*getSide();
    }


    @Override
    public String toString() {
        return "A Square with side = " + getSide() + ", which is a subclass of " + super.toString();
    }

    @Override
    public void setWidth(double side) {
        super.setLength(side);
        super.setWidth(side);
        this.side =side;
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
        super.setLength(side);
        this.side = side;
    }
}




