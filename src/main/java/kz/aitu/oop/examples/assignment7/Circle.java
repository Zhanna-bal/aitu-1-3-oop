package kz.aitu.oop.examples.assignment7;

 public class Circle extends Shape  {
    private double radius;

    public  Circle() {
        super();
        radius = 1.0;
    }

    public Circle (double radius){
        super();
        this.radius = radius;
    }

    public  Circle(double radius, String color, boolean filled) {
        super (color, filled);
        this.radius = radius;
    }

    public void setRadius(double radius) {this.radius = radius;
    }


   @Override
   public double getArea() {
        return radius*radius*Math.PI;
    }

    @Override
    public double getPerimeter() {
        return radius*2*Math.PI;
    }

    @Override
    public String toString() {
        return "A circle with radius = " + radius + " which is a subclass of " + super.toString();
    }

     @Override
     public double getLength() {
         return 0;
     }

     public double getRadius() {
        return 0;
    }
}
