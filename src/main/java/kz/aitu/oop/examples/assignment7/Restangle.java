package kz.aitu.oop.examples.assignment7;

public class Restangle extends Shape  {

    private double width;
    private double length;

    public Restangle() {

        width = 1.0;
        length= 1.0;
    }

    public Restangle (double width, double length) {
        super();
       this.width = width;
       this.length = length;
    }

    public Restangle(double width, double length,String color ,boolean filled) {
        super(color,filled);
        this.width = width;
        this.length = length;
    }


    @Override
    public double getArea() { return length*width;
    }

    @Override
    public double getPerimeter() { return (2*length)*(2*width);
    }

    @Override
    public String toString() {
        return "A restangle with width=" +width +"and lenght=" + length + " subclass of " +super.toString();
    }

    @Override
    public double getLength() {
        return 0;
    }


    protected void setLength(double side) {
    }

    protected void setWidth(double side) {
    }

    protected double getWidth() {

        return 0;
    }
}
