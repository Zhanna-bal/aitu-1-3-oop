package kz.aitu.oop.examples.encapsulation;

public class Main {


    public static void main(String[] args) {
        Account account = new Account();

        System.out.println(account.getCreditNumber());
        System.out.println(account.getBalance());

        account.addMoney(300);
        account.subtractMoney(900);
    }
}
