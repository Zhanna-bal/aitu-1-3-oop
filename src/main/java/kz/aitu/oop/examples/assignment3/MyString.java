package kz.aitu.oop.examples.assignment3;

import java.io.*;
import java.util.Scanner;


public class MyString {
    private int[] a;
    public MyString(int[] values) {
        a = values;
    }

    public int length() { return a.length; }

    public int valueAt(int position) {
        if (position > a.length) { return -1;}
        return a[position];
    }
    //return a position

    public boolean contains(int value) {
        for ( int i : a) {
            if (value == i) return true;
        }
        return false;
    }


    public int count(int value) {
        int count = 0;
        for(int i : a) {
            if (value == i) count++;
        }
        return count;
    }

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        File file = new File("C:\\Users\\User-PC\\Desktop\\zhanna.txt");
        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
        if (file.exists()) {
            outputStream.writeObject(arr);
            outputStream.close();
        }
        System.exit(0);

    }

}
