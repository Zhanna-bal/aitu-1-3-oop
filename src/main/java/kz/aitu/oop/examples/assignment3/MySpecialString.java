package kz.aitu.oop.examples.assignment3;

import static java.util.Arrays.sort;

public class MySpecialString {
    private int[] arr;

    MySpecialString(int[] values) {
        sort(values);
        int[] array = new int[values.length];
        int count = 0;
        for (int i = 0; i < values.length; i++ ) {
            if (i == 0) {
                array[count] = values[i];
                count++;
            } else if (values[i] != values[i-1]) {
                array[count] = values[i];
                count++;
            }
        }
        arr = new int[count];
        for (int i = 0; i < count; i++) {
            arr[i] = array[i];
        }
    }
    public int length() {
        return arr.length;
    }
    public int valueAt(int position) {
        if (position >= 0 && position < arr.length) {
            return arr[position];
        } else {
            return -1;
        }
    }

    public boolean contains(int value) {
        for (int i = 0; i < length(); i++ ) {
            if(arr[i] == value) {
                return true;
            }
        }
        return false;
    }

    public int count(int value) {
        int count = 0;
        for (int i = 0; i < length(); i++ ) {
            if(arr[i] == value) {
                count++;
            }
        }
        return count;
    }

    public void printValues() {
        for (int i = 0; i < arr.length; i++ ) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

}
