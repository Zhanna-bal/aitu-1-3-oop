package kz.aitu.oop.examples.assignment3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int b = sc.nextInt();
        int[] arr = new int[b];
        for (int i = 0; i < b; i++ ){
            arr[i] = sc.nextInt();
        }
        MySpecialString aa = new MySpecialString(arr);
        aa.printValues();
    }


}

