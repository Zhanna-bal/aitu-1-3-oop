package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.examples.Point;
import kz.aitu.oop.repository.StudentFileRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;

@RestController
@RequestMapping("/api/task/2")
public class AssignmentController2 {


    @GetMapping("/students")
    public ResponseEntity<?> getStudents() throws FileNotFoundException {

        //my code here
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String result = "";
        for (Student student: studentFileRepository.getStudents()) {
            for (Student student: studentJDBCRepository.getStudents()) {
            result += student.getName() + "\t" + student.getAge() + "\t" + student.getPoint() + "</br>";
        }

        return ResponseEntity.ok(result);
    }

    /**
     * Method get all Students from file and calculate average name ө
     * @throws FileNotFoundException
     */
    @GetMapping("/averageStudentsNameLength")
    public ResponseEntity<?> averageStudentsNameLength() throws FileNotFoundException {

        double average = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double total = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents()) {
            total += student.getName().length();
            count++;
        }
        average = total / count;

        return ResponseEntity.ok(average);
    }


    @GetMapping("/averageStudentsCount")
    public ResponseEntity<?> averageStudentsCount() throws FileNotFoundException {

        double average = 0;



        StudentFileRepository studentFileRepository = new StudentFileRepository();

        String group="";
        double ngroup = 0;
        double total = 0;
        int count = 0;

        for (Student student: studentFileRepository.getStudents()) {

            count++;

            if(!student.getGroup().equals(group)){

                group=student.getGroup();
                ngroup++;

            }
        }
        average = count/ngroup;

        return ResponseEntity.ok(average);

    }

    @GetMapping("/averageStudentsPoint")
    public ResponseEntity<?> averageStudentsPoint() throws FileNotFoundException {

        double average = 0;


        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double pointall = 0;
        int count = 0;
        for (Student student: studentFileRepository.getStudents() ) {
            pointall=pointall+student.getPoint();
            count++;
        }

        average=pointall/count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/averageStudentsAge")
    public ResponseEntity<?> averageStudentsAge() throws FileNotFoundException {

        double average = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        double age = 0;

        int count = 0;

        for (Student student: studentFileRepository.getStudents() ) {
            age = age+student.getAge();
            count++;
        }

        average=age / count;

        return ResponseEntity.ok(average);
    }

    @GetMapping("/highestStudentsPoint")
    public ResponseEntity<?> highestStudentsPoint() throws FileNotFoundException {

        double maxPoint = 0;

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for (Student student: studentFileRepository.getStudents() ) {
            if(maxPoint<student.getPoint()){
                maxPoint=student.getPoint();
            }
        }

        return ResponseEntity.ok(maxPoint);
    }

    @GetMapping("/highestStudentsAge")
    public ResponseEntity<?> highestStudentsAge() throws FileNotFoundException {

        double maxAge = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for (Student student: studentFileRepository.getStudents() ) {
            if(maxAge<student.getAge()){
                maxAge=student.getAge();
            }
        }

        return ResponseEntity.ok(maxAge);
    }

    @GetMapping("/highestGroupAveragePoint")
    public ResponseEntity<?> highestGroupAveragePoint() throws FileNotFoundException {

        double averageGroupPoint = 0;

        double first = 0;
        double second = 0;
        int count = 1;
        String group = "";

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for (Student student: studentFileRepository.getStudents() ) {

            if(student.getGroup().equals(group)) {
                count++;
                first += student.getPoint();
            }else{
                group=student.getGroup();
                if(second < first/count){
                    second = first/count;
                }
                count=1;
                first=student.getPoint();
            }
        }
        averageGroupPoint=second;


        return ResponseEntity.ok(averageGroupPoint);
    }

    @GetMapping("/highestGroupAverageAge")
    public ResponseEntity<?> highestGroupAverageAge() throws FileNotFoundException {

        double averageGroupAge = 0;
        double first = 0;
        double second = 0;
        int count=1;
        String group="";

        StudentFileRepository studentFileRepository = new StudentFileRepository();

        for (Student student: studentFileRepository.getStudents() ) {

            if(student.getGroup().equals(group)) {
                count++;
                first += student.getAge();
            }else{
                group=student.getGroup();
                if(second < first / count){
                    second = first / count;
                }
                count = 1;
                first = student.getAge();
            }
        }
        averageGroupAge = second;

        return ResponseEntity.ok(averageGroupAge);
    }


    @GetMapping("/all")
    public ResponseEntity<?> allData() throws FileNotFoundException {

        String result = "";
        result += "averageStudentsNameLength: " + averageStudentsNameLength().getBody() + "</br>";
        result += "averageStudentsCount: " + averageStudentsCount().getBody() + "</br>";
        result += "averageStudentsPoint: " + averageStudentsPoint().getBody() + "</br>";
        result += "averageStudentsAge: " + averageStudentsAge().getBody() + "</br>";
        result += "highestStudentsPoint: " + highestStudentsPoint().getBody() + "</br>";
        result += "highestStudentsAge: " + highestStudentsAge().getBody() + "</br>";
        result += "highestGroupAveragePoint: " + highestGroupAveragePoint().getBody() + "</br>";
        result += "highestGroupAverageAge: " + highestGroupAverageAge().getBody() + "</br>";

        return ResponseEntity.ok(result);
    }
}
